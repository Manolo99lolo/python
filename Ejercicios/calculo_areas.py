# coding: utf-8
from math import pi
area1 = (input ("¿Que figura quiere calcular ? [Escriba T o C]"))
if (area1== "T"):
    pregunta_area1 =  float(input("Escriba la base"))
    pregunta_area2 =  float(input("Escriba la altura"))
    resultado_T = (pregunta_area1 * pregunta_area2) / 2
    print("Un tríangulo de base",pregunta_area1,"y altura",pregunta_area2,"tiene un área de",resultado_T)
elif (area1 == "C") : 
    pregunta_circulo1 = float(input("Escriba el radio"))
    resultado_C = (2 * pregunta_circulo1* pi)
    print("Un circulo de radio",pregunta_circulo1,"tiene un área de",resultado_C )
