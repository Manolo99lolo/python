import tkinter 
from tkinter import *
from tkinter import ttk

# Variables

def boton():
    gasolina=lista_desplegable1.get()
    print(gasolina)
    tipogas=lista_desplegable2.get()
    print(tipogas)
    litros=float (numlitros.get())
    print(litros)

    if (gasolina=="super"):
        if (tipogas == "normal") : 
            resultado=(1.5 * (litros), '€') 
        elif(tipogas == "super") :
            resultado=( 1.55 * (litros), "€") 
    elif (gasolina=="sin plomo"):
        if (tipogas == "normal") : 
            resultado=(1.6 * (litros), '€') 
        elif(tipogas == "super") :
            resultado=( 1.65 * (litros), "€") 
    elif (gasolina=="diesel") :
        if (tipogas == "normal") : 
            resultado=(1.7 * (litros), '€') 
        elif(tipogas == "super") :
            resultado=( 1.75 * (litros), "€") 

    salidaNumero.delete(0, END)
    salidaNumero.insert(0, resultado)
















# Tkinter

ventana = tkinter.Tk()
ventana.resizable(0,0)
ventana.geometry("400x300")
etiqueta = tkinter.Label(ventana, text = "Gasolinera", bg = "blue")
etiqueta.pack(fill = tkinter.X)

# desplegable 1

lista_desplegable1=ttk.Combobox(ventana, width=14)
lista_desplegable1.place(x=15, y=50)
lista_desplegable1['values'] = ("super", "sin plomo", "diesel")
lista_desplegable1.current(0)

textoLista1=Label(ventana, text="Que gasolina quieres ?", font=("Berlin Sans FB", 12), bg="#AEC5CC")
textoLista1.place(x=15, y=25)

# Desplegable 2

lista_desplegable2=ttk.Combobox(ventana, width=14)
lista_desplegable2.place(x=260, y=50)
lista_desplegable2['values'] = ("super", "normal",)
lista_desplegable2.current(0)

textoLista2=Label(ventana, text="Que tipo ?", font=("Berlin Sans FB", 12), bg="#AEC5CC")
textoLista2.place(x=275, y=25)

# Input litros

numlitros=StringVar()
entradaNumero = Entry(ventana, textvariable=numlitros, font="Arial 14", width=20)
entradaNumero.place(x=15, y=120)

textoEntrada=Label(ventana, text="Cuantos litros quieres ?", font=("Berlin Sans FB", 12), bg="#AEC5CC")
textoEntrada.place(x=15, y=90)

# Boton

btnSubmit = Button(ventana, text = "Calcular", width = 8, height = 2, command = boton)
btnSubmit.place(x=150, y=240)

# Ventana resultado

salidaNumero = Entry(ventana, font="Arial 14", width=32)
salidaNumero.place(x=15, y=210)

textoSalida=Label(ventana, text="A Pagar", font=("Berlin Sans FB", 12), bg="#AEC5CC")
textoSalida.place(x=15, y=180)


ventana.mainloop()